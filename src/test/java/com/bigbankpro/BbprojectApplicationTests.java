package com.bigbankpro;

import com.bigbankpro.model.Account;
import com.bigbankpro.model.CreditCard;
import com.bigbankpro.service.AccountActivityService;
import com.bigbankpro.service.AccountService;
import com.bigbankpro.service.CreditCardMovementService;
import com.bigbankpro.service.CreditCardService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class BbprojectApplicationTests {

	@Autowired
	CreditCardService creditCardService;

	@Autowired
	CreditCardMovementService cardMovementService;

	@Autowired
	AccountService accountService;

	@Autowired
	AccountActivityService accountActivityService;

	String accNo1Esistente = "CC07800654";
	String accNo2Esistente ="CC78000089";
	String accNoNonEsistente ="CC78000000"; // non esiste ancora!

	String cCNo1Esistente = "EXS896"; //=> associata al conto account
	String cCNo2Esistente = "VEH897"; //=> associata al conto account2
	String cCNo3Esistente = "EX46TS"; //=> associata al conto account1
	String cCNoNoEsistente = "EX46TS";

	private Account account = new Account(accNo1Esistente, 234.78f);
	private Account account2 = new Account(accNo2Esistente, 67f);

	CreditCard creditCard = new CreditCard("EXS896", account);
	CreditCard creditCard2 = new CreditCard("VEH897", account2);
	CreditCard creditCard3= new CreditCard("EX46TS", account);


	@Test
	void testAddAccount(){

	}

	@Test
	void testAddCreditCard(){

	}

	@Test
	void testDebitAccountActivity() {

	}

	@Test
	void testCreditAccountActivity(){

	}

	@Test
	void testAddCreditCardMovement(){

	}

	@Test
	void contextLoads() {
	}

}
