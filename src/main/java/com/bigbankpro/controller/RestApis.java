package com.bigbankpro.controller;

import com.bigbankpro.exception.NoCoherentRequestException;
import com.bigbankpro.model.CreditCard;
import com.bigbankpro.service.AccountActivityService;
import com.bigbankpro.service.AccountService;
import com.bigbankpro.service.CreditCardMovementService;
import com.bigbankpro.service.CreditCardService;
import com.bigbankpro.utils.entityUtils.AllMonthlyAccountActivitiesAndTotalsUtils;
import com.bigbankpro.utils.entityUtils.CreditCardMonthlyMovements;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * Created by Padre Loco on 14/11/2019.
 * Copyright (c) 2019 All rights reserved.
 */

@RestController
@RequestMapping("/bigbank")
public class RestApis {

    @Autowired
    private AccountService accountService;

    @Autowired
    private AccountActivityService accountActivityService;

    @Autowired
    private CreditCardService creditCardService;

    @Autowired
    private CreditCardMovementService creditCardMovementService;


    /**
     * *** Uploading ******
     */

    @RequestMapping(value = "/uploadfile", method = RequestMethod.POST,
            consumes = MediaType.MULTIPART_FORM_DATA_VALUE)

    public ResponseEntity<Object> uploadFile(@RequestParam("file") MultipartFile multipartFile) throws IOException {

        if (multipartFile.isEmpty()) {
            throw new NoCoherentRequestException("Failed to store empty file");
        }

        String fileName = multipartFile.getOriginalFilename();
        File file = multipartToFile(multipartFile, fileName);
        String fileExtension = fileName.split("\\.")[1].toLowerCase();

        System.out.println( file.length());

        switch (fileExtension){
            case "csv":
                this.creditCardMovementService.uploadCreditCardMovements(file);
                break;
            case "txt":
                this.accountActivityService.uploadAccountMovements(file);
                break;
            case "xml":
                this.accountService.uploadAccountAndAssociatedCreditCard(file);
                break;
            default:
                throw new NoCoherentRequestException("Error! The file type (" + fileExtension.toLowerCase() +
                        ") is not supported!");
        }
        return new ResponseEntity<>("The file " + fileName + " is successfully uploaded.", HttpStatus.OK);
    }

    public File multipartToFile(MultipartFile multipart, String fileName) throws IllegalStateException, IOException {
        File convFile = new File(System.getProperty("java.io.tmpdir")+"/"+fileName);
        multipart.transferTo(convFile);
        return convFile;
    }


    /**
     * ******** Retrieval // Visualization *****
     */

    //I movimenti sui conti ed il loro totale

    @RequestMapping(value = "/account/movements", method = RequestMethod.GET,
            params = {"account", "month", "year"})
    public AllMonthlyAccountActivitiesAndTotalsUtils getAllMonthlyAccountActivitiesAndTotals(
            @RequestParam("account") String accNo,
            @RequestParam("month") int month,
            @RequestParam("year") int year){
        return this.accountActivityService.getAllMonthlyAccountActivitiesAndTotals(accNo, month, year, null);
    }

    @RequestMapping(value = "/account/movements", method = RequestMethod.GET,
            params = {"account", "month", "year", "sign"})
    public AllMonthlyAccountActivitiesAndTotalsUtils getAllMonthlyAccountActivitiesAndTotals(
            @RequestParam("account") String accNo,
            @RequestParam("month") int month,
            @RequestParam("year") int year,
            @RequestParam("sign") String sign){
        return this.accountActivityService.getAllMonthlyAccountActivitiesAndTotals(accNo, month, year, sign);
    }


    // I movimenti sulle carte associate al conto ed il loro totale
    @RequestMapping(value = "/card/movoments", method = RequestMethod.GET,
            params = {"cardnumber", "month", "year", "sign"})
    public CreditCardMonthlyMovements getCreditCardMonthlyMovements(
            @RequestParam("card") String cCNo,
            @RequestParam("month") int month,
            @RequestParam("year") int year){
        return this.creditCardMovementService.getCreditCardMonthlyMovements(cCNo, month, year);
    }

    @RequestMapping(value = "/account/cards", method = RequestMethod.GET)
    public List<CreditCard> getAssociatedCreditCards(@RequestParam("account") String accNo){
        return this.creditCardService.getAllCreditCardsOfAssociatedAccount(accNo);
    }

    @RequestMapping(method = RequestMethod.GET)
    public String greetings(){
        return "The endpoint is working well!";
    }

}
