package com.bigbankpro.repository;
import com.bigbankpro.model.Account;
import com.bigbankpro.model.CreditCard;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

/**
 * Created by Padre Loco on 12/11/2019.
 * Copyright (c) 2019 All rights reserved.
 */

public interface CreditCardRepository extends JpaRepository<CreditCard, String> {
    List<CreditCard> findByAccount(Account account);
    Optional<CreditCard> findByCcNo(String ccNo);

}
