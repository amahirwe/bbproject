package com.bigbankpro.repository;
import com.bigbankpro.model.CreditCard;
import com.bigbankpro.model.CreditCardMovement;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by Padre Loco on 12/11/2019.
 * Copyright (c) 2019 All rights reserved.
 */

public interface CreditCardMovementRepository extends JpaRepository<CreditCardMovement,Long>{

    List<CreditCardMovement> findByCreditCard(CreditCard creditCard);
    List<CreditCardMovement> findByCreditCardAndMonthAndYear(CreditCard creditCard, int month, int year);

}
