package com.bigbankpro.repository;

import com.bigbankpro.model.Account;
import com.bigbankpro.model.AccountActivity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * Created by Padre Loco on 12/11/2019.
 * Copyright (c) 2019 All rights reserved.
 */

public interface AccountActivityRepository extends JpaRepository<AccountActivity, Long>{

    List<AccountActivity> findByAccount(Account account);
    List<AccountActivity> findByAccountAndMonthAndYear(Account account, int month, int year);
    List<AccountActivity> findByAccountAndMonthAndYearAndSign(Account account, int month, int year, String sign);
}
