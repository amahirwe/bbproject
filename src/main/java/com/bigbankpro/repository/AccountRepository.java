package com.bigbankpro.repository;
import com.bigbankpro.model.Account;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

/**
 * Created by Padre Loco on 12/11/2019.
 * Copyright (c) 2019 All rights reserved.
 */



public interface AccountRepository extends JpaRepository<Account, String> {

    Optional<Account> findByAccNo(String accNo);
}
