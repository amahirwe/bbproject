package com.bigbankpro.service;

import com.bigbankpro.model.Account;
import com.bigbankpro.model.AccountActivity;
import com.bigbankpro.exception.NoCoherentRequestException;
import com.bigbankpro.model.CreditCard;
import com.bigbankpro.model.CreditCardMovement;
import com.bigbankpro.repository.AccountActivityRepository;
import com.bigbankpro.utils.ParseFiles;
import com.bigbankpro.utils.ValidationUtils;
import com.bigbankpro.utils.entityUtils.AccountActivityUtils;
import com.bigbankpro.utils.entityUtils.AccountMonthlyCreditedActivitiesUtils;
import com.bigbankpro.utils.entityUtils.AllMonthlyAccountActivitiesAndTotalsUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by Padre Loco on 12/11/2019.
 * Copyright (c) 2019 All rights reserved.
 * Obiettivo: - pottere produre i movimenti sui conti ed il loro totale
 */

@Service
public class AccountActivityService {

    @Autowired
    private AccountActivityRepository  accountActivityRepository;

    @Autowired
    private AccountService accountService;

    @Autowired
    private CreditCardService creditCardService;

    @Autowired
    private CreditCardMovementService creditCardMovementService;



    /**
     *  GETTERS ------------------------------------------------------------   ***
     */

    public List<AccountActivity> getAllAccountActivities(String accNo){
        ValidationUtils.checkAccountNumber(accNo);
        Account account = this.accountService.getAccountByAccNo(accNo);
        List<AccountActivity> accountActivities = new ArrayList<>();
        accountActivities.addAll(this.accountActivityRepository.findByAccount(account));
        return accountActivities;
    }

    public List<AccountActivity> getMonthlyAccountActivities(String accNo, int month, int year){
        ValidationUtils.checkAccountNumber(accNo);
        ValidationUtils.checkMonth(month);
        ValidationUtils.checkYear(year);
        ValidationUtils.checkDateValidity(year, month, 1);
        Account account = this.accountService.getAccountByAccNo(accNo);
        List<AccountActivity> accountMonthlyActivities = new ArrayList<>();
        accountMonthlyActivities.addAll(this.accountActivityRepository.findByAccountAndMonthAndYear(account, month, year));
        return accountMonthlyActivities;
    }

    public List<AccountActivity> getMonthlyAccountActivitiesBySign(String accNo, int month, int year, String sign){
        ValidationUtils.checkAccountNumber(accNo);
        ValidationUtils.checkMonth(month);
        ValidationUtils.checkYear(year);
        ValidationUtils.checkSign(sign);
        Account account = this.accountService.getAccountByAccNo(accNo);
        List<AccountActivity> accountMonthlyActivities = new ArrayList<>();
        accountMonthlyActivities.addAll(this.accountActivityRepository.findByAccountAndMonthAndYearAndSign(account, month, year, sign));
        return accountMonthlyActivities;
    }

    public float monthlyAccountTotalBySign(String ccNo, int month, int year, String sign){
        ValidationUtils.checkCreditCardNumber(ccNo);
        ValidationUtils.checkMonth(month);
        ValidationUtils.checkYear(year);
        ValidationUtils.checkDateValidity(year, month, 1);
        ValidationUtils.checkSign(sign);

        List<AccountActivity> monthlyAccountActivitiesBySign = this.getMonthlyAccountActivitiesBySign(ccNo, month, year,sign);

        if (monthlyAccountActivitiesBySign.size() < 1) return 0;

        else{
            float total = 0;
            for (AccountActivity accountActivity : monthlyAccountActivitiesBySign) {
                total += accountActivity.getAmount();
            }
            return total;
        }
    }


    public AccountMonthlyCreditedActivitiesUtils getAccountMonthlyCreditedActivitiesUtils(String accNo, int month, int year){
        List<AccountActivity> accountCreditActivities = this.getMonthlyAccountActivitiesBySign(accNo, month, year, "CR");
        float totalCredited = this.monthlyAccountTotalBySign(accNo, month, year, "CR");
        return new AccountMonthlyCreditedActivitiesUtils(accountCreditActivities, totalCredited);
    }

    public AllMonthlyAccountActivitiesAndTotalsUtils getAllMonthlyAccountActivitiesAndTotals(String accNo, int month, int year, String sign){
        List<AccountActivity> accountCreditActivities = new ArrayList<>();
        float totalCreditedAmount = 0f;

        List<CreditCardMovement> creditCardMovements = new ArrayList<>();
        float creditCardDebitTotals = 0f;
        List<AccountActivity> accountDebitActivities = new ArrayList<>();
        float accountDebitTotals = 0f;
        float totalDebitedAmount = 0f;
        Account account = this.accountService.getAccountByAccNo(accNo);
        float currentBalance = account.getAvailAmount();
        List<CreditCard> associatedCreditCards = this.creditCardService.getAllCreditCardsOfAssociatedAccount(accNo);


        if (sign != null){
            ValidationUtils.checkSign(sign);
            switch (sign.toUpperCase()){
                case "CR":{
                    accountCreditActivities = this.getMonthlyAccountActivitiesBySign(accNo, month, year, sign);
                    totalCreditedAmount = this.monthlyAccountTotalBySign(accNo, month, year, sign);

                    return new AllMonthlyAccountActivitiesAndTotalsUtils(accountCreditActivities,
                            totalCreditedAmount,creditCardMovements, creditCardDebitTotals,
                            accountDebitActivities, accountDebitTotals, totalDebitedAmount, currentBalance);

                }
                case "DR": {
                    for (CreditCard creditCard: associatedCreditCards) {
                        creditCardMovements.addAll(this.creditCardMovementService.getMonthlyCreditCardActivities(creditCard.getCcNo(),month, year));
                        creditCardDebitTotals += this.creditCardMovementService.monthlyCreditCardTotal(creditCard.getCcNo(),month, year);
                    }

                    accountDebitActivities = this.getMonthlyAccountActivitiesBySign(accNo, month, year, sign);
                    totalDebitedAmount = this.monthlyAccountTotalBySign(accNo, month, year, sign);

                    return new AllMonthlyAccountActivitiesAndTotalsUtils(accountCreditActivities,
                            totalCreditedAmount,creditCardMovements, creditCardDebitTotals,
                            accountDebitActivities, accountDebitTotals, totalDebitedAmount, currentBalance);
                }
                default:
                    throw new NoCoherentRequestException("Error: for the 'sign' value choose either 'CR' or 'DR'" );
            }
        }
        accountCreditActivities = this.getMonthlyAccountActivities(accNo, month, year);
        totalCreditedAmount = this.monthlyAccountTotalBySign(accNo,month, year, "CR");

        for (CreditCard creditCard: associatedCreditCards) {
            creditCardMovements.addAll(this.creditCardMovementService.getMonthlyCreditCardActivities(accNo,month, year));
            creditCardDebitTotals += this.creditCardMovementService.monthlyCreditCardTotal(creditCard.getCcNo(),month, year);
        }
        accountDebitActivities = this.getMonthlyAccountActivities(accNo, month, year);
        totalDebitedAmount = creditCardDebitTotals + totalCreditedAmount;


        return new AllMonthlyAccountActivitiesAndTotalsUtils(accountCreditActivities,totalCreditedAmount,
                creditCardMovements, creditCardDebitTotals, accountDebitActivities, accountDebitTotals,
                totalDebitedAmount, account.getAvailAmount());

    }


    /**
     * MODIFIERS ------------------------------------------------------------   ***
     */

    public void uploadAccountMovements(File file){

        List<AccountActivityUtils> accountActivityUtilsList = new ArrayList<>(ParseFiles.getAccActivitiesFromTxt(file));

        for (AccountActivityUtils accountActivityUtils : accountActivityUtilsList) {
            Account account = this.accountService.getAccountByAccNo(accountActivityUtils.getAccNo());
            int day = accountActivityUtils.getDay();
            int month = accountActivityUtils.getMonth();
            int year = accountActivityUtils.getYear();
            float activityAmount = accountActivityUtils.getAmount();
            String sign = accountActivityUtils.getSign();
            String activityText = accountActivityUtils.getActivityText();
            AccountActivity newAccountActivity = new AccountActivity(account, day, month, year,
                    activityAmount, sign, activityText);

            switch(sign) {
                case "CR":
                    this.creditAccount(account, activityAmount);
                    this.saveAccountMovement(newAccountActivity);
                    break;
                case "DR":
                    this.debitAccount(account,activityAmount);
                    this.saveAccountMovement(newAccountActivity);
                    break;
                default:
                    throw new NoCoherentRequestException("The accounting entry provided " + sign +
                            " is wrong. \n Values allowed: 'DR' and 'CR'." );
            }
        }


    }

    private void creditAccount(Account account, float transactionAmount){
        account.setAvailAmount(account.getAvailAmount() + transactionAmount);
        this.updateAccount(account);
    }

    private void debitAccount(Account account, float transactionAmount){
        float currBalance = account.getAvailAmount();
        if (currBalance < transactionAmount)
            throw new NoCoherentRequestException("Insufficient amount (" + currBalance + ") on account " +
                    account.getAccNo());
        else{
            account.setAvailAmount(currBalance - transactionAmount);
            this.updateAccount(account);
        }
    }

    private void updateAccount(Account account){
        this.accountService.updateAccountBalance(account);
    }

    private void saveAccountMovement(AccountActivity accountActivity){
        this.accountActivityRepository.save(accountActivity);
    }
    /* Obiettivo: - pottere produre i movimenti sui conti ed il loro totale */
}

