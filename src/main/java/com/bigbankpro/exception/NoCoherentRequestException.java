package com.bigbankpro.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by Padre Loco on 11/11/2019.
 * Copyright (c) 2019 All rights reserved.
 */


@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class NoCoherentRequestException extends RuntimeException {
    public NoCoherentRequestException(){
        super("**** Request NOT coherent ***");
    }

    public NoCoherentRequestException(String paramType, String param, String suggestion){
        super("**** " + paramType + " provided: " + param + " is not coherent! " + suggestion + " ***");
    }

    public NoCoherentRequestException(String paramType, String param){
        super("**** " + paramType + " provided: " + param + " already exists in the system! ***");
    }

    public NoCoherentRequestException(String message){
        super("**** " + message + " ***");
    }
}
