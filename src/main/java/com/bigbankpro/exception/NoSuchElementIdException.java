package com.bigbankpro.exception;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by Padre Loco on 11/11/2019.
 * Copyright (c) 2019 All rights reserved.
 */

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class NoSuchElementIdException extends RuntimeException{
    public NoSuchElementIdException(String elementName, String id){
        super("*** " + elementName + " with number {" + id + "} was found! ***" );
    }

    public NoSuchElementIdException(String message){
        super(message);
    }
}
