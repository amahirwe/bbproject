package com.bigbankpro.utils.entityUtils;


/**
 * Created by Padre Loco on 13/11/2019.
 * Copyright (c) 2019 All rights reserved.
 */


public class CreditCardMovementUtils {

    private String cCNo;
    private int day;
    private int month;
    private int year;
    private float Amount;

    public CreditCardMovementUtils(String cCNo, int day, int month, int year, float amount) {
        this.cCNo = cCNo;
        this.day = day;
        this.month = month;
        this.year = year;
        Amount = amount;
    }

    public CreditCardMovementUtils() {
    }

    public String getcCNo() {
        return cCNo;
    }

    public void setcCNo(String cCNo) {
        this.cCNo = cCNo;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public float getAmount() {
        return Amount;
    }

    public void setAmount(float amount) {
        Amount = amount;
    }
}
