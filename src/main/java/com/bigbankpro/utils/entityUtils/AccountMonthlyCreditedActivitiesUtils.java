package com.bigbankpro.utils.entityUtils;
import com.bigbankpro.model.AccountActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Padre Loco on 14/11/2019.
 * Copyright (c) 2019 All rights reserved.
 */


public class AccountMonthlyCreditedActivitiesUtils {
    private List<AccountActivity> accountCreditActivities = new ArrayList<>();
    private float totalCredited;


    public AccountMonthlyCreditedActivitiesUtils(List<AccountActivity> accountCreditActivities, float totalCredited) {
        this.accountCreditActivities = accountCreditActivities;
        this.totalCredited = totalCredited;
    }

    public List<AccountActivity> getAccountCreditActivities() {
        return accountCreditActivities;
    }

    public void setAccountCreditActivities(List<AccountActivity> accountCreditActivities) {
        this.accountCreditActivities = accountCreditActivities;
    }

    public float getTotalCredited() {
        return totalCredited;
    }

    public void setTotalCredited(float totalCredited) {
        this.totalCredited = totalCredited;
    }
}
