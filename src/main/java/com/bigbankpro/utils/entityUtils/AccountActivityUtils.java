package com.bigbankpro.utils.entityUtils;
/**
 * Created by Padre Loco on 14/11/2019.
 * Copyright (c) 2019 All rights reserved.
 */


public class AccountActivityUtils {
    private String accNo;
    private int day;
    private int month;
    private int year;
    private float amount;
    private String sign;
    private String activityText;

    public AccountActivityUtils() {
    }

    public AccountActivityUtils(String accNo,
                                int day, int month, int year, float amount, String sign, String activityText) {
        this.accNo = accNo;
        this.day = day;
        this.month = month;
        this.year = year;
        this.amount = amount;
        this.sign = sign;
        this.activityText = activityText;
    }

    public String getAccNo() {
        return accNo;
    }

    public void setAccNo(String accNo) {
        this.accNo = accNo;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public float getAmount() {
        return amount;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public String getActivityText() {
        return activityText;
    }

    public void setActivityText(String activityText) {
        this.activityText = activityText;
    }
}