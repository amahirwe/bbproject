package com.bigbankpro.utils.entityUtils;


import com.bigbankpro.model.AccountActivity;
import com.bigbankpro.model.CreditCardMovement;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Padre Loco on 14/11/2019.
 * Copyright (c) 2019 All rights reserved.
 */



public class AllMonthlyAccountActivitiesAndTotalsUtils {

    private List<AccountActivity> accountCreditActivities = new ArrayList<>();
    private float tatalCreditedAmount;

    private List<CreditCardMovement> creditCardMovements = new ArrayList<>();
    private float creditCardDebitTotals;

    private List<AccountActivity> accountDebitActivities = new ArrayList<>();
    private float accountDebitTotals;

    private float totalDebitedAmount;

    private float availableBalance;


    public AllMonthlyAccountActivitiesAndTotalsUtils(List<AccountActivity> accountCreditActivities, float tatalCreditedAmount, List<CreditCardMovement> creditCardMovements, float creditCardDebitTotals, List<AccountActivity> accountDebitActivities, float accountDebitTotals, float totalDebitedAmount, float availableBalance) {
        this.accountCreditActivities = accountCreditActivities;
        this.tatalCreditedAmount = tatalCreditedAmount;
        this.creditCardMovements = creditCardMovements;
        this.creditCardDebitTotals = creditCardDebitTotals;
        this.accountDebitActivities = accountDebitActivities;
        this.accountDebitTotals = accountDebitTotals;
        this.totalDebitedAmount = totalDebitedAmount;
        this.availableBalance = availableBalance;
    }

    public List<AccountActivity> getAccountCreditActivities() {
        return accountCreditActivities;
    }

    public void setAccountCreditActivities(List<AccountActivity> accountCreditActivities) {
        this.accountCreditActivities = accountCreditActivities;
    }

    public float getTatalCreditedAmount() {
        return tatalCreditedAmount;
    }

    public void setTatalCreditedAmount(float tatalCreditedAmount) {
        this.tatalCreditedAmount = tatalCreditedAmount;
    }

    public List<AccountActivity> getAccountDebitActivities() {
        return accountDebitActivities;
    }

    public void setAccountDebitActivities(List<AccountActivity> accountDebitActivities) {
        this.accountDebitActivities = accountDebitActivities;
    }

    public List<CreditCardMovement> getCreditCardMovements() {
        return creditCardMovements;
    }

    public void setCreditCardMovements(List<CreditCardMovement> creditCardMovements) {
        this.creditCardMovements = creditCardMovements;
    }

    public float getCreditCardDebitTotals() {
        return creditCardDebitTotals;
    }

    public void setCreditCardDebitTotals(float creditCardDebitTotals) {
        this.creditCardDebitTotals = creditCardDebitTotals;
    }

    public float getAccountDebitTotals() {
        return accountDebitTotals;
    }

    public void setAccountDebitTotals(float accountDebitTotals) {
        this.accountDebitTotals = accountDebitTotals;
    }

    public float getTotalDebitedAmount() {
        return totalDebitedAmount;
    }

    public void setTotalDebitedAmount(float totalDebitedAmount) {
        this.totalDebitedAmount = totalDebitedAmount;
    }

    public float getAvailableBalance() {
        return availableBalance;
    }
}
