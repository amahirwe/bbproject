package com.bigbankpro.utils.entityUtils;
import com.bigbankpro.model.AccountActivity;
import com.bigbankpro.model.CreditCardMovement;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Padre Loco on 14/11/2019.
 * Copyright (c) 2019 All rights reserved.
 */

public class MonthlyAccountDebitedActivities {

    private List<CreditCardMovement> creditCardDebitedMovements = new ArrayList<>();
    private float creditCardDebitedMovementsTotals;

    private List<AccountActivity> accountDebitedActivities = new ArrayList<>();
    private float accountDebitedActivitiesTotal;

    private float totalDebitedAmount;

    public MonthlyAccountDebitedActivities(List<CreditCardMovement> creditCardDebitedMovements, float creditCardDebitedMovementsTotals, List<AccountActivity> accountDebitedActivities, float accountDebitedActivitiesTotal, float totalDebitedAmount) {
        this.creditCardDebitedMovements = creditCardDebitedMovements;
        this.creditCardDebitedMovementsTotals = creditCardDebitedMovementsTotals;
        this.accountDebitedActivities = accountDebitedActivities;
        this.accountDebitedActivitiesTotal = accountDebitedActivitiesTotal;
        this.totalDebitedAmount = totalDebitedAmount;
    }

    public List<CreditCardMovement> getCreditCardDebitedMovements() {
        return creditCardDebitedMovements;
    }

    public void setCreditCardDebitedMovements(List<CreditCardMovement> creditCardDebitedMovements) {
        this.creditCardDebitedMovements = creditCardDebitedMovements;
    }

    public float getCreditCardDebitedMovementsTotals() {
        return creditCardDebitedMovementsTotals;
    }

    public void setCreditCardDebitedMovementsTotals(float creditCardDebitedMovementsTotals) {
        this.creditCardDebitedMovementsTotals = creditCardDebitedMovementsTotals;
    }

    public List<AccountActivity> getAccountDebitedActivities() {
        return accountDebitedActivities;
    }

    public void setAccountDebitedActivities(List<AccountActivity> accountDebitedActivities) {
        this.accountDebitedActivities = accountDebitedActivities;
    }

    public float getAccountDebitedActivitiesTotal() {
        return accountDebitedActivitiesTotal;
    }

    public void setAccountDebitedActivitiesTotal(float accountDebitedActivitiesTotal) {
        this.accountDebitedActivitiesTotal = accountDebitedActivitiesTotal;
    }

    public float getTotalDebitedAmount() {
        return totalDebitedAmount;
    }

    public void setTotalDebitedAmount(float totalDebitedAmount) {
        this.totalDebitedAmount = totalDebitedAmount;
    }
}
