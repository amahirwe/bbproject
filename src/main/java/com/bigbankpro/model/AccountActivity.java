package com.bigbankpro.model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Created by Padre Loco on 11/11/2019.
 * Copyright (c) 2019 All rights reserved.
 */

@Entity
public class AccountActivity {


    @Id
    private Long activityId;

    @ManyToOne
    @NotNull
    @JoinColumn(name = "accNo")
    private Account account;

    @NotNull
    @Column(updatable = false)
    private int day;
    @NotNull
    @Column(updatable = false)
    private int month;
    @NotNull
    @Column(updatable = false)
    private int year;

    @NotNull
    private float amount;

    @NotNull
    private String sign;

    @NotNull
    private String activityText;

    public AccountActivity() {
    }

    public Long getActivityId() {
        return activityId;
    }

    public AccountActivity(@NotNull Account account,
                           @NotNull int day,
                           @NotNull int month,
                           @NotNull int year,
                           @NotNull float amount,
                           @NotNull String sign,
                           @NotNull String activityText)
    {
        this.account = account;
        this.day = day;
        this.month = month;
        this.year = year;
        this.amount = amount;
        this.sign = sign;
        this.activityText = activityText;
    }

    public void setActivityId(Long activityId) {
        this.activityId = activityId;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public float getAmount() {
        return amount;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public String getActivityText() {
        return activityText;
    }

    public void setActivityText(String activityText) {
        this.activityText = activityText;
    }
}
