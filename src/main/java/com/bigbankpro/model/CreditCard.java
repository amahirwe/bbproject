package com.bigbankpro.model;

import javax.persistence.Entity;
import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Created by Padre Loco on 11/11/2019.
 * Copyright (c) 2019 All rights reserved.
 */

@Entity
public class CreditCard {

    @Id
    private String ccNo;

    @ManyToOne
    @JoinColumn(name = "accNo")
    @NotNull
    private Account account;

    public CreditCard() {
    }

    public CreditCard(String ccNo, @NotNull Account account) {
        this.ccNo = ccNo;
        this.account = account;
    }

    public String getCcNo() {
        return ccNo;
    }

    public void setCcNo(String ccNo) {
        this.ccNo = ccNo;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }
}
